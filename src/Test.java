import java.util.HashSet;
import java.util.Iterator;

public class Test {
    public static void main(String[] args) {
        HashSet<String> h = new HashSet<>();
        String s1 = "qqq";
        String s2 = "www";
        String s3 = "www";

        if (h.add(s1)) System.out.println(s1 + " ok"); else System.out.println(s1 + " error");
        if (h.add(s2)) System.out.println(s2 + " ok"); else System.out.println(s2 + " error");
        if (h.add(s3)) System.out.println(s3 + " ok"); else System.out.println(s3 + " error"); //нельзя хранить одинаковые объекты
        System.out.println(h); //вывод в консоль - 1 вариант
        for (String v : h) { //2 вариант
            System.out.println(v);
        }
        Iterator i = h.iterator(); //3 вариант
        while (i.hasNext()) {
            System.out.println(i.next());
        }
        h.forEach(System.out::println); //Java 8+

        System.out.println( h.contains(s3) ); //в наборе содержится "www"
        System.out.println( h.contains("xxx") );
        System.out.println(h.size());

        h.remove(s2);
        System.out.println(h);

        h.add("zzz");
        h.clear();
        System.out.println(h);

        class Rect {
            int width;
            int height;

            Rect(int width, int height) {
                this.width = width;
                this.height = height;
            }

            int area() {
                return width * height;
            }

            @Override
            public String toString() {
                return String.format("%dx%d=%d", width, height, area());
            }

            @Override
            public int hashCode() {
                return Integer.decode(Integer.toString(width) + Integer.toString(height));
            }

            @Override
            public boolean equals(Object obj) {
                return width == ((Rect)obj).width && height == ((Rect)obj).height;
            }
        }

        HashSet<Rect> r = new HashSet<>();
        r.add(new Rect(1, 10));
        r.add(new Rect(5, 1));
        r.add(new Rect(5, 1));
        System.out.println(r);
    }
}
